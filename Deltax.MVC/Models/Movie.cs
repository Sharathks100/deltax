﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Deltax.MVC.Models
{
	public class Movie
	{
		public int MovieId { get; set; }
		public string Name { get; set; }
		public Nullable<System.DateTime> ReleaseDate { get; set; }
		public string Plot { get; set; }
		public byte[] Poster { get; set; }
		public int ProducerId { get; set; }
		public int[] ActorIds { get; set; }
	}
}