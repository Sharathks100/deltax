﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Deltax.MVC.Models
{
	public class Producer
	{
		public int ProducerId { get; set; }
		public string Name { get; set; }
		public Nullable<int> SexId { get; set; }
		public string BioData { get; set; }
		public Nullable<System.DateTime> DOB { get; set; }
	}
}