﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DeltaX.Data;

namespace Deltax.MVC.Controllers
{
	public class MoviesController : Controller
	{


		// GET: Movies
		public ActionResult Index()
		{
			MovieContext db = new MovieContext();
			IEnumerable<Movie> movies;
			using (var UnitofWork = new UnitofWork(db))
			{
				movies = UnitofWork.movieRepository.GetAllDetails().ToList();
			}

			return View(movies);
		}

		public ActionResult GetMovies()
		{
			MovieContext db = new MovieContext();
			IEnumerable<Movie> movies;
			using (var UnitofWork = new UnitofWork(db))
			{
				movies = UnitofWork.movieRepository.GetAllDetails().ToList();

			}

			var moviedetails = movies.Select(x => new
			{
				movieid = x.MovieId,
				moviename = x.Name,
				plot = x.Plot,
				poster = x.Poster,
				producername = x.Producer.Name,
				producerid = x.ProducerId,
				releasedate = x.ReleaseDate.HasValue?x.ReleaseDate.Value.ToShortDateString():DateTime.MinValue.ToShortDateString(),
				actors = x.MovieActors != null && x.MovieActors.Count > 0 ? string.Join(",", x.MovieActors.Select(y => y.Actor.Name).ToList()) : null
			});

			return new JsonResult() { Data = moviedetails, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		// GET: Movies/Details/5
		public ActionResult GetMovieDetail(int? id)
		{
			MovieContext db = new MovieContext();
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Movie movie = db.Movies.Find(id);

			using (var UnitofWork = new UnitofWork(db))
			{
				movie = UnitofWork.movieRepository.GetMovieDetail(id.Value);

			}
			if (movie == null)
			{
				return HttpNotFound();
			}
			var actorIds = movie.MovieActors != null && movie.MovieActors.Count > 0 ? movie.MovieActors.Select(y => new { actorid = y.ActorId, actorname = y.Actor.Name }).ToArray() : null;

			var moviedetail = new
			{
				MovieId = movie.MovieId,
				Name = movie.Name,
				ReleaseDate = movie.ReleaseDate.HasValue ? movie.ReleaseDate.Value.ToShortDateString() : DateTime.MinValue.ToShortDateString(),
				ProducerId = movie.ProducerId,
				ProducerName = movie.Producer.Name,
				MovieActors = actorIds
			};
			return new JsonResult() { Data = moviedetail, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		// GET: Movies/Create
		public ActionResult GetAllProducers()
		{
			MovieContext db = new MovieContext();
			IEnumerable<Producer> producers;
			using (var UnitofWork = new UnitofWork(db))
			{
				producers = UnitofWork.producerRepository.GetAll().ToList();
			}
			return new JsonResult() { Data = producers.Select(x => new { ProducerId = x.ProducerId, Name = x.Name }), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public ActionResult GetAllActors()
		{
			MovieContext db = new MovieContext();
			IEnumerable<Actor> acters;
			using (var UnitofWork = new UnitofWork(db))
			{
				acters = UnitofWork.actorRepository.GetAll().ToList();

			}
			return new JsonResult() { Data = acters.Select(x => new { ActorId = x.ActorId, Name = x.Name }), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public ActionResult GetSexdetail()
		{
			MovieContext db = new MovieContext();
			IEnumerable<Sex> sexes;
			using (var UnitofWork = new UnitofWork(db))
			{
				sexes = UnitofWork.sexRepository.GetAll().ToList();

			}
			return new JsonResult() { Data = sexes.Select(x => new { SexId = x.SexId, Sex = x.Sex1 }), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}


		[HttpPost]

		public ActionResult SaveMovie([Bind(Include = "MovieId,Name,ReleaseDate,Plot,Poster,ProducerId,ActorIds")] Models.Movie movie)
		{
			MovieContext db = new MovieContext();
			Movie movieToSave = null;

			using (var UnitofWork = new UnitofWork(db))

			{
				if (movie.MovieId > 0)
				{
					movieToSave = UnitofWork.dbContext.Movies.Single(p => p.MovieId == movie.MovieId);

					movieToSave.Name = movie.Name;
					movieToSave.ReleaseDate = movie.ReleaseDate;
					movieToSave.Plot = movie.Plot;
					movieToSave.Poster = movie.Poster;
					movieToSave.ProducerId = movie.ProducerId;
					foreach (var item in movie.ActorIds)
					{
						if ((movieToSave.MovieActors.FirstOrDefault(x => x.ActorId == item) == null))
							movieToSave.MovieActors.Add(new MovieActor() { ActorId = item, MovieId = movie.MovieId });
					}
					UnitofWork.movieRepository.Update(movieToSave);
					UnitofWork.dbContext.Entry(movieToSave).State = EntityState.Modified;
				}
				else
				{
					movieToSave = new Movie();
					movieToSave.Name = movie.Name;
					movieToSave.Plot = movie.Plot;
					movieToSave.Poster = movie.Poster;
					movieToSave.ProducerId = movie.ProducerId;
					movieToSave.ReleaseDate = movie.ReleaseDate;
					foreach (var item in movie.ActorIds)
					{
						movieToSave.MovieActors.Add(new MovieActor() { ActorId = item, MovieId = movie.MovieId });
					}

					UnitofWork.dbContext.Entry(movieToSave).State = EntityState.Added;
					UnitofWork.movieRepository.Add(movieToSave);
				}

				int i = UnitofWork.Complete();
			}

			return new JsonResult() { Data = null, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		// GET: Movies/Edit/5
		public ActionResult Edit(int? id)
		{
			MovieContext db = new MovieContext();
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Movie movie = db.Movies.Find(id);
			if (movie == null)
			{
				return HttpNotFound();
			}
			ViewBag.ProducerId = new SelectList(db.Producers, "ProducerId", "Name", movie.ProducerId);
			return View(movie);
		}

		public ActionResult AddActor([Bind(Include = "Name,SexId,DOB,BioData")] Models.Actor actor)
		{
			MovieContext db = new MovieContext();
			Actor a = new Actor();
			a.Name = actor.Name;
			a.SexId = actor.SexId;
			a.DOB = actor.DOB;
			a.BioData = actor.BioData;
			using (var UnitofWork = new UnitofWork(db))

			{
				UnitofWork.dbContext.Entry(a).State = EntityState.Added;
				UnitofWork.actorRepository.Add(a);
				int i = UnitofWork.Complete();
			}
			return new JsonResult() { Data = new { ActorId = a.ActorId, Name = a.Name }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public ActionResult AddProducer([Bind(Include = "Name,SexId,DOB,BioData")] Models.Producer producer)
		{
			MovieContext db = new MovieContext();
			Producer a = new Producer();
			a.Name = producer.Name;
			a.SexId = producer.SexId;
			a.DOB = producer.DOB;
			a.BioData = producer.BioData;
			using (var UnitofWork = new UnitofWork(db))

			{
				UnitofWork.dbContext.Entry(a).State = EntityState.Added;
				UnitofWork.producerRepository.Add(a);
				int i = UnitofWork.Complete();
			}
			return new JsonResult() { Data = new { ProducerId = a.ProducerId, Name = a.Name }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

	}
}
