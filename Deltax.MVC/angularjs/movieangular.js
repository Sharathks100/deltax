﻿var movieManager = (function () {

	var initialise = function () {
		var moviemodule = angular.module("Movie", ['ngRoute']);

		moviemodule.config(
			function ($routeProvider) {
				$routeProvider.
					when('/EditMovie/:movieId', {
						templateUrl: 'Edit.html',
						controller: 'AngularController'
					})
			});
		moviemodule.service('movieService', ['$http', function ($http) {
			return {
				GetAllMovies: function () {

					return $http.get('/movies/GetMovies');

				}
				,
				GetMovieDetail: function (id) {

					return $http(
						{
							method: "GET",
							url: '/Movies/GetMovieDetail/' + id

						}
					);
				},
				GetAllProducers: function () { return $http.get('/movies/GetAllProducers'); },
				GetAllActors: function () { return $http.get('/movies/GetAllActors'); },

				SaveMovie: function (selectedmovie) {

					var actorIds = selectedmovie.MovieActors.map(a => a.ActorId);
					return $http(
						{
							method: "POST",
							url: '/Movies/SaveMovie/',
							params: {
								MovieId: selectedmovie.MovieId, Name: selectedmovie.Name, ReleaseDate: selectedmovie.ReleaseDate
								, Plot: selectedmovie.Plot, Poster: selectedmovie.Poster, ProducerId: selectedmovie.ProducerId, ActorIds: actorIds
							}
						}
					);
				},
				GetSexdetail: function () { return $http.get('/movies/GetSexdetail'); },

				AddActor: function (addActor) {

					return $http(
						{
							method: "POST",
							url: '/Movies/AddActor/',
							params: {
								Name: addActor.Name, DOB: addActor.DOB
								, BioData: addActor.BioData, SexId: addActor.SexId
							}
						}
					);
				},
				AddProducer: function (addProducer) {

					return $http(
						{
							method: "POST",
							url: '/Movies/AddProducer/',
							params: {
								Name: addProducer.Name, DOB: addProducer.DOB
								, BioData: addProducer.BioData, SexId: addProducer.SexId
							}
						}
					);
				}

			}

		}]);
		moviemodule.controller('movieController', ['$scope', 'movieService', '$window', '$routeParams',
			function ($scope, movieService, $window, $routeParams) {
				$scope.movies = [];
				$scope.producers = [];
				$scope.actors = [];
				$scope.sexes = [];
				$scope.showedit = false;
				$scope.selectedmovie = {};
				$scope.addActorProducer = {};
				$scope.opts = {
					dateFormat: 'dd/mm/yy',
					changeMonth: true,
					changeYear: true
				};
				//id: null, name="", releasedate=null, poster=null, plot: null, producerid=null, producername: null, actorids: []
				$scope.getAllmovies = function () {
					movieService.GetAllMovies().then(function (result) {

						$scope.movies = result.data;
					});
				};

				$scope.getMovieDetail = function (id) {
					$scope.showedit = true;
					movieService.GetMovieDetail(id).then(function (result) {


						$scope.selectedmovie = result.data;
					});
				};
				$scope.getAllProducers = function () {
					movieService.GetAllProducers().then(function (result) {

						$scope.producers = result.data;
					});
				};

				$scope.getAllActors = function () {
					movieService.GetAllActors().then(function (result) {

						$scope.actors = result.data;
					});
				};

				$scope.getSexdetail = function () {
					movieService.GetSexdetail().then(function (result) {

						$scope.sexes = result.data;
					});
				};

				$scope.saveMovie = function () {
					movieService.SaveMovie($scope.selectedmovie).then(function (result) {
						scope.getAllmovies();
						$scope.showedit = false;
						$scope.showAddProducer = false;
					});
				};

				$scope.addActor = function () {
					movieService.AddActor($scope.addActorProducer).then(function (result) {
						$scope.showedit = true;
						$scope.showAddProducer = false;
						$scope.actors.push(result.data);

					});
				};

				$scope.addProducer = function () {
					movieService.AddProducer($scope.addActorProducer).then(function (result) {
						$scope.showedit = true;
						$scope.showAddProducer = false;
						$scope.producers.push(result.data);
					});
				};

				$scope.cancel = function () {
					$scope.showAddProducer = false;
				};

				$scope.AddNewActorProducer = function () {
					$scope.showAddProducer = true;
					$scope.showedit = false;
				};


			}]);

		moviemodule.directive('ngEnter', function () {
			return function (scope, element, attrs) {
				element.bind("keydown keypress", function (event) {
					if (event.which === 13) {
						scope.$apply(function () {
							scope.$eval(attrs.ngEnter);
						});

						event.preventDefault();
					}
				});
			};
		});
	};

	return {
		init: function () {

			console.info('movie.init');
			initialise();
		}
	};

}());



