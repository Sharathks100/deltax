﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public interface IUnitofWork : IDisposable
	{
		IActorRepository actorRepository { get; }
		IProducerRepository producerRepository { get; }
		IMovieRepository movieRepository { get; }

		ISexRepository sexRepository { get; }

		int Complete();
	}
}
