﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public interface ISexRepository : IGenericRepository<Sex>
	{
	}
}
