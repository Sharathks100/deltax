﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public class UnitofWork : IUnitofWork
	{
		public readonly MovieContext dbContext;
		public UnitofWork(MovieContext dbContext)
		{
			this.dbContext = dbContext;
			this.actorRepository = new ActorRepository(dbContext);
			this.movieRepository = new MovieRepository(dbContext);
			this.producerRepository = new ProducerRepository(dbContext);
			this.sexRepository = new SexRepository(dbContext);
		}
		public IActorRepository actorRepository { get; }

		public IProducerRepository producerRepository { get; }

		public IMovieRepository movieRepository { get; }

		public ISexRepository sexRepository { get; }

		public int Complete()
		{
			return this.dbContext.SaveChanges();
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					//	dbContext.Dispose();
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~UnitofWork() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
