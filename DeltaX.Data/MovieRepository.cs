﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public class MovieRepository : GenericRepository<Movie>, IMovieRepository
	{
		public MovieRepository(MovieContext dbContext) : base(dbContext)
		{
		}

		public MovieContext MovieDbContext { get { return dbContext as MovieContext; } }

		public Movie Findmovie(int movieid)
		{
			return MovieDbContext.Movies.Where(x => x.MovieId == movieid).FirstOrDefault();
		}

		public IEnumerable<Movie> GetAllDetails()
		{
			return MovieDbContext.Movies.Include(DeltaX => DeltaX.MovieActors)
				.Include(x => x.Producer);
		}

		public Movie GetMovieDetail(int movieid)
		{
			return MovieDbContext.Movies.Include(DeltaX => DeltaX.MovieActors).Include(x => x.Producer).Where(x => x.MovieId == movieid).FirstOrDefault();
		}
	}


}
