﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public class ActorRepository : GenericRepository<Actor>, IActorRepository
	{
		public ActorRepository(MovieContext dbContext) : base(dbContext)
		{
		}

		public MovieContext MovieDbContext { get { return dbContext as MovieContext; } }
	}
}
