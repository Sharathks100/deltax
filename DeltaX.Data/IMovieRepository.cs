﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public interface IMovieRepository:IGenericRepository<Movie>
	{
		IEnumerable<Movie> GetAllDetails();
		Movie GetMovieDetail(int movieid);

		Movie Findmovie(int movieid);
	}
}
