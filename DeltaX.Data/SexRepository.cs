﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public class SexRepository : GenericRepository<Sex>, ISexRepository
	{
		public SexRepository(MovieContext dbContext) : base(dbContext)
		{
		}

		public MovieContext MovieDbContext { get { return dbContext as MovieContext; } }
	}
}
