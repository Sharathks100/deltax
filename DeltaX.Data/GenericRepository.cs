﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
	{
		protected readonly DbContext dbContext;

		public GenericRepository(DbContext dbContext)
		{
			this.dbContext = dbContext;
		}

		public void Add(TEntity entity)
		{
			this.dbContext.Set<TEntity>().Add(entity);
		}

		public void Delete(TEntity entity)
		{
			this.dbContext.Set<TEntity>().Remove(entity);
		}

		public IEnumerable<TEntity> GetAll()
		{
			return this.dbContext.Set<TEntity>().ToList();
		}

		public IEnumerable<TEntity> GetEntities(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate)
		{
			return this.dbContext.Set<TEntity>().Where(predicate).ToList();
		}

		public TEntity Update(TEntity entity)
		{
			return this.dbContext.Set<TEntity>().Attach(entity);
		}
	}

	
}
