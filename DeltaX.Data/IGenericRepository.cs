﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public interface IGenericRepository<TEntity> where TEntity : class
	{
		IEnumerable<TEntity> GetAll();
		IEnumerable<TEntity> GetEntities(Expression<Func<TEntity,bool>> predicate);

		void Add(TEntity entity);

		TEntity Update(TEntity entity);

		void Delete(TEntity entity);
	}
}
