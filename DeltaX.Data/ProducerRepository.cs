﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaX.Data
{
	public class ProducerRepository : GenericRepository<Producer>, IProducerRepository
	{
		public ProducerRepository(DeltaEntities dbContext) : base(dbContext)
		{
		}
		public DeltaEntities MovieDbContext { get { return dbContext as DeltaEntities; } }
	}
}
