﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DeltaX.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DeltaX.Controllers
{
	public class MovieController : Controller
	{
		private IMovieContext movieContext;
		public MovieController(IMovieContext movieContext)
		{
			var services = new ServiceCollection();

			this.movieContext = movieContext;

		}
		public IActionResult Index()
		{
			var builder = new ConfigurationBuilder()
		   .SetBasePath(Directory.GetCurrentDirectory())
		   .AddJsonFile("appsettings.json");

			var configuration = builder.Build();
			var v = configuration.GetConnectionString("DeltaEntities1");
			IEnumerable<Movie> lst = null;
			using (var unitofWork = new UnitofWork(movieContext as MovieContext))
			{
				lst = unitofWork.movieRepository.GetAll();
			}
			return View(lst);
		}
	}
}