USE [master]
GO

/****** Object:  Database [Delta]    Script Date: 24/05/2019 7:03:59 p.m. ******/
CREATE DATABASE [Delta]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Delta', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Delta.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Delta_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Delta_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [Delta] SET COMPATIBILITY_LEVEL = 120
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Delta].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [Delta] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [Delta] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [Delta] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [Delta] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [Delta] SET ARITHABORT OFF 
GO

ALTER DATABASE [Delta] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [Delta] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [Delta] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [Delta] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [Delta] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [Delta] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [Delta] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [Delta] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [Delta] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [Delta] SET  DISABLE_BROKER 
GO

ALTER DATABASE [Delta] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [Delta] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [Delta] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [Delta] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [Delta] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [Delta] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [Delta] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [Delta] SET RECOVERY FULL 
GO

ALTER DATABASE [Delta] SET  MULTI_USER 
GO

ALTER DATABASE [Delta] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [Delta] SET DB_CHAINING OFF 
GO

ALTER DATABASE [Delta] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [Delta] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [Delta] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [Delta] SET  READ_WRITE 
GO


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE TABLE Sex
(
SexId Int NOT NULL IDENTITY(1,1),
Sex VARCHAR(15),
PRIMARY KEY(SexId)
);

CREATE TABLE Actors (
    ActorId int NOT NULL IDENTITY(1,1) ,
    Name VARCHAR(100),
    SexId int,
	BioData VARCHAR(MAX),
	PRIMARy KEY(ActorId),
	FOREIGN KEY(SexId) REFERENCES Sex(Sexid)
);

CREATE TABLE Producer(
	ProducerId int NOT NULL IDENTITY(1,1),
    Name VARCHAR(100),
    SexId int,
	BioData VARCHAR(MAX),
	PRIMARy KEY(ProducerId),
	FOREIGN KEY(SexId) REFERENCES Sex(Sexid)
);

CREATE TABLE Movie(
	MovieId int NOT NULL IDENTITY(1,1) ,
    Name VARCHAR(100),
    ReleaseDate DATE,
    Plot VARCHAR(MAX),
    Poster image,
	ProducerId int
	PRIMARy KEY(MovieId),
	FOREIGN KEY(ProducerId) REFERENCES Producer(ProducerId)
	
);

 CREATE TABLE MovieActor(
 MovieId INT NOT NULL,
 ActorId INT NOT NULL,
 FOREIGN KEY(MovieId) REFERENCES Movie( MovieId),
 FOREIGN KEY(ActorId) REFERENCES Actors(ActorId)
  );

ALTER TABLE MovieActor
ADD MovieActorId INT NOT NULL IDENTITY(1,1) 

ALTER TABLE MovieActor
ADD CONSTRAINT PK_MovieActorId PRIMARY KEY(MovieActorId)

ALTER TABLE Actors
ADD  DOB DATE ;

ALTER TABLE Producer
ADD  DOB DATE ;


  INSERT INTO Sex(Sex) VALUES('Male'),('Female'),('Others');

  INSERT INTO Actors(Name,SexId,BioData,DOB )
  VALUES('ABC',3,'qweeeqewqe','1970-01-01 00:00:00'),
  ('Salman khan',1,'qweeeqewqe','1969-01-01 00:00:00'),
  ('Akshay',1,'qweeeqewqe','1974-01-01 00:00:00'),
  ('Amitab',1,'qweeeqewqe','1955-01-01 00:00:00'),
  ('Mahesh babu',1,'qweeeqewqe','1980-01-01 00:00:00'),
  ('Sudeep',1,'qweeeqewqe','1971-01-01 00:00:00'),
  ('Deepika',2,'qweeeqewqe','1985-01-01 00:00:00'),
  ('Rashmika',2,'qweeeqewqe','1989-01-01 00:00:00'),
  ('Rajnikant',1,'qweeeqewqe','1965-01-01 00:00:00'),
  ('Dr Rajkumar',1,'qweeeqewqe','1945-01-01 00:00:00');


   INSERT INTO Producer(Name,SexId,BioData,DOB )
  VALUES('ABC',1,'qweeeqewqe','1970-01-01 00:00:00'),
  ('XYZ',1,'qweeeqewqe','1969-01-01 00:00:00'),
  ('Mr Y',1,'qweeeqewqe','1974-01-01 00:00:00'),
  ('Mr X',1,'qweeeqewqe','1955-01-01 00:00:00'),
  ('ms Q',1,'qweeeqewqe','1980-01-01 00:00:00'),
  ('Karan',1,'qweeeqewqe','1971-01-01 00:00:00'),
  ('Pasfdg',2,'qweeeqewqe','1985-01-01 00:00:00'),
  ('WER',2,'qweeeqewqe','1989-01-01 00:00:00'),
  ('qwerty',1,'qweeeqewqe','1965-01-01 00:00:00'),
  ('afdfsdr',1,'qweeeqewqe','1945-01-01 00:00:00');

  INSERT INTO Movie(Name,Plot,ProducerId,ReleaseDate)
  VALUES('First movie','qweeeqewqe',1,'1978-01-01 00:00:00'),
  ('second movie','qweeeqewqe',2,'1989-01-01 00:00:00'),
  ('Third movie','qweeeqewqe',3,'2006-01-01 00:00:00'),
  ('forth movie','qweeeqewqe',4,'1999-01-01 00:00:00'),
  ('Fifth movie','qweeeqewqe',5,'1998-01-01 00:00:00')
  
  INSERT INTO Movie(Name,Plot,ProducerId,ReleaseDate)
  VALUES('First movie_1','qweeeqewqe',2,'1978-01-01 00:00:00')

   INSERT INTO Movie(Name,Plot,ProducerId,ReleaseDate)
  VALUES('First movie_1','qweeeqewqe',1,'1979-01-01 00:00:00')




